import React, {Component} from "react";
import List from '../ListReact/List'
import NewEmployee from '../NewEmployee'

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            items: [],
            trigger: 0
        };
    }

    editTrigger = () => {
        this.setState({trigger: !this.state.trigger})
    };

    componentDidMount() {
        fetch("http://127.0.0.1:8080/empl_list/api/v1.0/all")
            .then(res => res.json())
            .then(
                result => {
                    this.setState({
                        items: result
                    });
                }
            );
    }

    componentWillUpdate(prevProps, prevState) {
        if (this.state.trigger !== prevState.trigger) {
            fetch("http://127.0.0.1:8080/empl_list/api/v1.0/all")
                .then(res => res.json())
                .then(
                    result => {
                        this.setState({
                            items: result
                        });
                    }
                );
        }
    }

    render() {
        return (
            <React.Fragment>
                <table className="table table-sm">
                    <List items={this.state.items} editTrigger={this.editTrigger}/>
                    <tr>
                        <NewEmployee editTrigger={this.editTrigger}/>
                    </tr>
                </table>
            </React.Fragment>

        )
    }
}
