import React, {Component} from 'react';
import "./Buttons.css"

export default class Buttons extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addButton: props.addButton,
            addMode: false,
            editButton: props.editButton,
            editMode: false,
        };
    }


    onEditButtonClick = () => {
        this.props.editItem();
        return (
            this.setState({editMode: true})
        )
    };


    onCancelButtonClick = () => {
        if (this.state.editMode) {
            this.props.editItem();
            this.setState({editMode: false})
        } else if (this.state.addMode) {
            this.props.addItem();
            this.setState({addMode: false})
        }
    };


    onSaveButtonClick = () => {
        if (this.state.editMode) {
            if (this.props.checkEqual()) {
                this.onCancelButtonClick()
            } else {
                this.props.sendData();
                this.onCancelButtonClick()
            }
        }
        if (this.state.addMode) {
            this.props.sendData();
            this.onCancelButtonClick();
        }
    };


    onDeleteButtonClick = () => {
        this.props.delData();
    };


    onAddButtonClick = () => {
        this.props.addItem();
        return (
            this.setState({addMode: true})
        )
    };


    buttons = {
        edit: <button type="button"
                      title='Edit'
                      className="btn btn-sm btn-outline-secondary"
                      onClick={this.onEditButtonClick}><i className='fas fa-pen'/>
        </button>,

        save: <button type="button"
                      title='Save'
                      className="btn btn-sm btn-outline-success"
                      onClick={this.onSaveButtonClick}><i className='fa fa-save'/>
        </button>,

        cancel: <button type="button"
                        title='Cancel'
                        className="btn btn-sm btn-outline-primary"
                        onClick={this.onCancelButtonClick}><i className='fa fa-close'/>
        </button>,

        delete: <button type="button"
                        title='Remove'
                        className="btn btn-sm btn-outline-danger"
                        onClick={this.onDeleteButtonClick}><i className='fa fa-trash'/>
        </button>,


        add: <button type="button"
                     className="btn btn-sm btn-outline-secondary"
                     title='Add new employee'
                     onClick={this.onAddButtonClick}><i className='fa fa-plus'/>
        </button>

    };


    renderButtons = () => {
        if (this.state.editButton) {
            if (this.state.editMode) {
                return [this.buttons.save, this.buttons.cancel, this.buttons.delete]
            } else {
                return this.buttons.edit
            }
        } else if (this.state.addButton) {
            if (this.state.addMode) {
                return [this.buttons.save, this.buttons.cancel]
            } else {
                return this.buttons.add
            }
        }
    };


    render() {

        return (
            <td>
                <form id='actions'>{this.renderButtons()}</form>
            </td>
        );
    }


}


