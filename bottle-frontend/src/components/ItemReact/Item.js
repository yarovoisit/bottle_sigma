import React, {Component} from 'react';
import "./Item.css"
import Buttons from '../ButtonsReact'


export default class Item extends Component {


    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            name: props.name,
            department: props.department_id,
            position: props.position_id,
            onEdit: false
        };
    }

    editItem = () => {
        this.setState({onEdit: !this.state.onEdit})
    };

    onNameChange = (label) => {
        this.setState({name: label.target.value})
    };

    onPositionChange = (label) => {
        this.setState({position: label.target.value})
    };

    onDepartmentChange = (label) => {
        this.setState({department: label.target.value})
    };

    checkEqual = () => {

        const deepEqual = (obj1, obj2) => {
            return JSON.stringify(obj1) === JSON.stringify(obj2);
        };

        const curProps = {
            // id: this.props.id,
            name: this.props.name,
            department: this.props.department_id,
            position: this.props.position_id
        };
        const curState = {
            // id: this.state.id,
            name: this.state.name,
            department: this.state.department,
            position: this.state.position
        };
        return deepEqual(curProps, curState)
    };

    putData = () => {
        const curState = {
            // id: this.state.id,
            name: this.state.name,
            department: this.state.department,
            position: this.state.position
        };
        fetch("http://localhost:8080/empl_list/api/v1.0/all/" + this.props.id, {
            method: "PUT",
            headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
            body: JSON.stringify(curState)
        });
    };

    delData = () => {
        fetch("http://localhost:8080/empl_list/api/v1.0/all/" + this.state.id, {method: 'DELETE'}).then(() => this.props.editTrigger());
    };


    renderData = () => {
        if (this.state.onEdit) {
            return (
                [<th>{this.state.id}</th>,
                    <td><input id='name' type='text' className='form-control' value={this.state.name} form='actions'
                               onChange={this.onNameChange}/></td>,
                    <td><input id='position' type='text' className='form-control' value={this.state.position} form='actions'
                               onChange={this.onPositionChange}/></td>,
                    <td><input id='department' type='text' className='form-control' value={this.state.department} form='actions'
                               onChange={this.onDepartmentChange}/></td>]
            )
        } else {
            return (
                [<th>{this.state.id}</th>,
                    <td>{this.state.name}</td>,
                    <td>{this.state.position}</td>,
                    <td>{this.state.department}</td>]
            );
        }

    };


    render() {
        return (
            [this.renderData(),
                <Buttons editButton={true} editItem={this.editItem} itemId={this.state.name} checkEqual={this.checkEqual} sendData={this.putData}
                         delData={this.delData}/>]
        )

    };
}


