import React, {Component} from 'react';
import Buttons from '../ButtonsReact'


export default class NewEmployee extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: null,
            position: null,
            department: null,
            onAdd: false
        };
    }

    addItem = () => {
        this.setState({onAdd: !this.state.onAdd})
    };

    onNameChange = (label) => {
        this.setState({name: label.target.value})
    };

    onPositionChange = (label) => {
        this.setState({position: label.target.value})
    };

    onDepartmentChange = (label) => {
        this.setState({department: label.target.value})
    };

    postData = () => {
        const curState = {
            // id: this.state.id,
            name: this.state.name,
            department: this.state.department,
            position: this.state.position
        };
        fetch("http://127.0.0.1:8080/empl_list/api/v1.0/all", {
            method: "POST",
            headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
            body: JSON.stringify(curState)
        }).then(() => this.props.editTrigger());
    };

    renderData = () => {
        if (this.state.onAdd) {
            return (
                [<th>{this.props.nextIndex}</th>,
                    <td><input id='name' type='text' className='form-control' placeholder='Name' form='actions'
                               onChange={this.onNameChange}/></td>,
                    <td><input id='position' type='text' className='form-control' placeholder='Position' form='actions'
                               onChange={this.onPositionChange}/></td>,
                    <td><input id='department' type='text' className='form-control' placeholder='Department' form='actions'
                               onChange={this.onDepartmentChange}/></td>]
            )
        } else {
            return [<th></th>, <td></td>, <td></td>, <td></td>]
        }

    };

    render() {
        return (
            [this.renderData(),
                <Buttons key='new_item' addButton={true} addItem={this.addItem} sendData={this.postData}/>]

        )
    }
}
