import React, {Component} from 'react';
import Item from '../ItemReact'
import './List.css';


export default class List extends Component {

    constructor(props) {
        super(props);
        this.state = {
            eventAdd: false,
        };
    }


    render() {
        const {items} = this.props;
        return (

            <React.Fragment>
                <thead>
                <tr>
                    <th>ID</th>
                    <th className="align-middle">Name</th>
                    <th>Position</th>
                    <th>Department</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {items.map((item) => {
                    return (
                        <tr key={this.state.id}>
                            <Item {...item} key={item.id} index={item.id} editTrigger={this.props.editTrigger}/>
                        </tr>
                    )
                })}
                </tbody>
            </React.Fragment>

        );
    }
}

