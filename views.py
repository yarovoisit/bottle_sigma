from bottle import json_dumps, request, HTTPResponse, HTTPError
from models import Department, Position, Employee
from settings import session


# CORS Handling ########################################################################################################
def handle_method_OPTIONS():
    return HTTPResponse(status=200,
                        headers={'Access-Control-Allow-Origin': '*',
                                 'Access-Control-Allow-Headers': '*',
                                 'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS'})


# Views ################################################################################################################
# GET Request
def get_all_employees():
    if request.method == 'OPTIONS':
        return handle_method_OPTIONS()
    elif request.method == 'GET':
        query = session.query(Employee).order_by(Employee.name).all()
        all_employees = list()
        for i in query:
            i.__dict__.update(position_id=i.position.name, department_id=i.department.name)
            all_employees.append(i.as_json)
        return HTTPResponse(status=200,
                            body=json_dumps(all_employees),
                            headers={'Content-Type': 'application/json',
                                     'Access-Control-Allow-Origin': '*'})


# POST Request
def create_employee():
    if request.method == 'OPTIONS':
        return handle_method_OPTIONS()
    elif request.method == 'POST':
        employee = request.json.get('name')
        department = session.query(Department).filter(Department.name == request.json.get('department')).first()
        position = session.query(Position).filter(Position.name == request.json.get('position')).first()
        if all([employee, department, position]):
            employee = Employee(name=employee, department=department, position=position)
            session.add(employee)
            session.commit()
            return HTTPResponse(status=201,
                                # body={'user_id': employee.id},
                                body='Employee was created successfully',
                                headers={'Content-Type': 'application/json',
                                         'Access-Control-Allow-Origin': '*'})
        else:
            return HTTPError(status=500,
                             body='Employee creation error',
                             # exception='Employee creation error',
                             headers={'Content-Type': 'application/json'})


# PUT Request
def edit_employee(id):
    if request.method == 'OPTIONS':
        return handle_method_OPTIONS()
    elif request.method == 'PUT':
        employee = session.query(Employee).filter(Employee.id == id).first()
        if employee:
            name = request.json.get('name')
            position = request.json.get('position')
            department = request.json.get('department')
            if all([name, position, department]):
                position = session.query(Position).filter(Position.name == position).first()
                department = session.query(Department).filter(Department.name == department).first()
                employee.position = position
                employee.department = department
                employee.name = name
                session.commit()
                return HTTPResponse(status=201,
                                    body='Employee was changed successfully',
                                    headers={'Content-Type': 'application/json',
                                             'Access-Control-Allow-Origin': '*'})
            else:
                return HTTPError(status=500,
                                 body='Employee changing error',
                                 # exception='Employee removal error',
                                 headers={'Content-Type': 'application/json'})
        else:
            return HTTPError(status=404,
                             body='Employee not found',
                             # exception='Employee not found',
                             headers={'Content-Type': 'application/json'})


# DELETE Request
def delete_employee(id):
    if request.method == 'OPTIONS':
        return handle_method_OPTIONS()
    elif request.method == 'DELETE':
        employee = session.query(Employee).filter(Employee.id == id).first()
        if employee:
            session.delete(employee)
            session.commit()
            return HTTPResponse(status=200,
                                body='Employee was deleted successfully',
                                headers={'Content-Type': 'application/json',
                                         'Access-Control-Allow-Origin': '*'})
        else:
            return HTTPError(status=500,
                             body='Employee removal error',
                             # exception='Employee removal error',
                             headers={'Content-Type': 'application/json'})
