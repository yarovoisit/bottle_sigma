from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from bottle.ext.sqlalchemy import Plugin
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('postgresql+psycopg2://bottle:bottle@localhost:5432/bottle')
Base = declarative_base()
plugin = Plugin(engine, keyword='db')
Session = sessionmaker(bind=engine)
session = Session()

# if __name__ == '__main__':
#     Base.metadata.create_all(engine)
