from views import get_all_employees, create_employee, edit_employee, delete_employee
from boddle import boddle
from unittest import TestCase, main as unittests_run
from models import Position, Department, Employee
from settings import session


class TestAPI(TestCase):

    def setUp(self):
        department = session.query(Department).filter(Department.name == 'web').first()
        position = session.query(Position).filter(Position.name == 'developer').first()
        employee = Employee(name='test_developer', department=department, position=position)
        session.add(employee)
        session.commit()
        self.test_data = employee

    def tearDown(self):
        if self._testMethodName == 'test_200_delete_employee':
            return None
        else:
            session.delete(self.test_data)
            session.commit()

    def test_200_get_all_employees(self):
        with boddle(path='/empl_list/api/v1.0/all',
                    method='GET'):
            test_resp = get_all_employees()
            self.assertEqual(test_resp.status_code, 200)
            self.assertEqual(test_resp.content_type, 'application/json')
            self.assertEqual('Access-Control-Allow-Origin' in test_resp.headers.dict.keys(), True)
            self.assertEqual(len(test_resp.body) > 0, True)

    def test_201_create_employee(self):
        with boddle(path='/empl_list/api/v1.0/all',
                    json={'name': 'test_manager', 'position': 'manager', 'department': 'eco'},
                    method='POST'):
            test_resp = create_employee()
            self.assertEqual(test_resp.status_code, 201)
        session.delete(session.query(Employee).filter(Employee.name == 'test_manager').first())
        session.commit()

    def test_500_create_employee(self):
        with boddle(path='/empl_list/api/v1.0/all',
                    json=self.test_data.as_json,
                    method='POST'):
            test_resp = create_employee()
            self.assertEqual(test_resp.status_code, 500)

    def test_201_edit_employee(self):
        self.test_data.name = 'test_worker'
        with boddle(path='/empl_list/api/v1.0/all/{}'.format(self.test_data.id),
                    json={'name': 'test_worker', 'position': 'worker', 'department': 'energy'},
                    method='PUT'):
            test_resp = edit_employee(self.test_data.id)
            self.assertEqual(test_resp.status_code, 201)

    def test_500_edit_employee(self):
        self.test_data.name = 'test_worker'
        with boddle(path='/empl_list/api/v1.0/all/{}'.format(self.test_data.id),
                    json=self.test_data.as_json,
                    method='PUT'):
            test_resp = edit_employee(self.test_data.id)
            self.assertEqual(test_resp.status_code, 500)

    def test_404_edit_employee(self):
        self.test_data.name = 'test_worker'
        with boddle(path='/empl_list/api/v1.0/all/{}'.format(self.test_data.id),
                    json={'name': 'test_worker', 'position': 'worker', 'department': 'energy'},
                    method='PUT'):
            test_resp = edit_employee(None)
            self.assertEqual(test_resp.status_code, 404)

    def test_200_delete_employee(self):
        with boddle(path='/empl_list/api/v1.0/all/{}'.format(self.test_data.id),
                    method='DELETE'):
            test_resp = delete_employee(self.test_data.id)
        self.assertEqual(test_resp.status_code, 200)

    def test_500_delete_employee(self):
        with boddle(path='/empl_list/api/v1.0/all/{}'.format(self.test_data.id),
                    method='DELETE'):
            test_resp = delete_employee(None)
        self.assertEqual(test_resp.status_code, 500)


if __name__ == '__main__':
    unittests_run()
