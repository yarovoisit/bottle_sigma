"""create all tables

Revision ID: 2a09bbf126b3
Revises: 7241c010c5ff
Create Date: 2019-11-04 16:02:06.011924

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2a09bbf126b3'
down_revision = '7241c010c5ff'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
