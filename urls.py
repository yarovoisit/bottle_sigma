from views import get_all_employees, edit_employee, create_employee, delete_employee


def start_routing(application):
    application.route('/empl_list/api/v1.0/all', method=['GET', 'OPTIONS'], callback=get_all_employees)
    application.route('/empl_list/api/v1.0/all', method=['POST', 'OPTIONS'], callback=create_employee)
    application.route('/empl_list/api/v1.0/all/<id>', method=['DELETE', 'OPTIONS'], callback=delete_employee)
    application.route('/empl_list/api/v1.0/all/<id>', method=['PUT', 'OPTIONS'], callback=edit_employee)
