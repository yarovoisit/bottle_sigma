from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from settings import Base


class Position(Base):
    __tablename__ = 'positions'
    id = Column(Integer, primary_key=True)
    name = Column(String(30), unique=True)
    employees = relationship('Employee', backref='position')


class Department(Base):
    __tablename__ = 'departments'
    id = Column(Integer, primary_key=True)
    name = Column(String(30), unique=True)
    employees = relationship('Employee', backref='department')


class Employee(Base):
    __tablename__ = 'employees'
    id = Column(Integer, primary_key=True)
    name = Column(String(30))
    position_id = Column(ForeignKey(Position.id))
    department_id = Column(ForeignKey(Department.id))

    @property
    def as_json(self):
        json = dict()
        for i in self.__dict__:
            if isinstance(self.__dict__[i], int) or isinstance(self.__dict__[i], str):
                json[i] = self.__dict__[i]
        return json
