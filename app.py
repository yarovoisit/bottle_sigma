from bottle import Bottle
from settings import plugin
import urls

app = Bottle()
app.install(plugin)
urls.start_routing(app)

# Main Programm ########################################################################################################
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
